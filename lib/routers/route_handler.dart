import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import '../pages/details_page.dart'; //引入页面

Handler detailsHandler = new Handler(
  handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    String goodsId = params['id'][0];
    print('index>detail goodsId is ${goodsId}');
    return DetailsPage(goodsId);
  },
);
