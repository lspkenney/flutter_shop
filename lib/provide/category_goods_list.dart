import 'package:flutter/material.dart';
import 'package:flutter_shop/model/categoryGoodsList.dart';

class CategoryGoodsListProvide extends ChangeNotifier {
  List<CategoryListData> goodsList = [];

  //点击大类更换商品列表
  setGoodsList(List<CategoryListData> list) {
    goodsList = list;
    notifyListeners();
  }
  //上拉加载更多商品列表
  setMoreGoodsList(List<CategoryListData> list) {
    goodsList.addAll(list);
    notifyListeners();
  }
}
