import 'package:flutter/material.dart';

///提供小部件可以重建对实现可监听接口的任何类的更改。
///在这里，我们混合使用ChangeNotifier，这样我们就不需要自己管理监听器了。
///扩展ValueNotifier <int>将是另一种简单的方法。
class Counter with ChangeNotifier {
  int _value;

  int get value => _value;//get函数

  Counter(this._value);//构造函数

  void increment() {//方法
    _value++;
    notifyListeners();
  }
}
