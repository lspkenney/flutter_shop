import 'package:flutter/material.dart';
import 'package:provide/provide.dart';
import 'package:flutter_shop/provide/details_info.dart';

//用于屏幕适配
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailsTopArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provide<DetailsInfoProvide>(builder: (context, child, val) {
      var goodsInfo = val.goodsInfo.data.goodInfo;
      if (goodsInfo != null) {
        return Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              _goodsImage(goodsInfo.image1),
              _goodsName(goodsInfo.goodsName),
              _goodsNumber(goodsInfo.goodsSerialNumber),
              _goodsPrice(goodsInfo.presentPrice, goodsInfo.oriPrice),
            ],
          ),
        );
      } else {
        return Text('正在加载中');
      }
    });
  }

  //商品图图片控件
  Widget _goodsImage(String url) {
    return Image.network(
      url,
      width: ScreenUtil.getInstance().setWidth(740),
    );
  }

  //商品名称控件
  Widget _goodsName(String name) {
    return Container(
      width: ScreenUtil.getInstance().setWidth(740),
      padding: const EdgeInsets.only(left: 15.0),
      child: Text(
        name,
        style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(30)),
      ),
    );
  }

  //商品编号
  Widget _goodsNumber(String number) {
    return Container(
      width: ScreenUtil.getInstance().setWidth(740),
      padding: const EdgeInsets.only(left: 15.0),
      margin: EdgeInsets.only(top: 8.0),
      child: Text(
        '编号：${number}',
        style: TextStyle(color: Colors.black26),
      ),
    );
  }

  //商品价格
  Widget _goodsPrice(double presentPrice, double oriPrice) {
    return Container(
      width: ScreenUtil.getInstance().setWidth(740),
      padding: const EdgeInsets.only(left: 15.0),
      margin: const EdgeInsets.only(top: 8.0),
      child: Row(
        children: <Widget>[
          Text(
            '￥${presentPrice}',
            style: TextStyle(
                color: Colors.pinkAccent,
                fontSize: ScreenUtil.getInstance().setSp(40)),
          ),
          Text(
            '市场价：￥${oriPrice}',
            style: TextStyle(
              color: Colors.black26,
              decoration: TextDecoration.lineThrough,
            ),
          )
        ],
      ),
    );
  }
}
