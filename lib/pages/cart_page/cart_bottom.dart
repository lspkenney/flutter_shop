import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_shop/provide/cart.dart';
import 'package:provide/provide.dart';

class CartBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5.0),
      color: Colors.white,
      child: Provide<CartProvide>(builder: (context, child, carInfo) {
        return Row(
          children: <Widget>[
            _selectAllButton(context),
            _totalPrice(context),
            _goButton(context),
          ],
        );
      }),
    );
  }

  //全选按钮
  Widget _selectAllButton(context) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: Provide.value<CartProvide>(context).isAllCheck,
          activeColor: Colors.pink,
          onChanged: (bool val) {
            Provide.value<CartProvide>(context).changeAllCheckSate(val);
          },
        ),
        Text('全选'),
      ],
    );
  }

  //总计
  Widget _totalPrice(context) {
    double totalPrice = Provide.value<CartProvide>(context).totalPrice;
    return Container(
      width: ScreenUtil.getInstance().setWidth(430),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                alignment: Alignment.centerRight,
                width: ScreenUtil.getInstance().setWidth(280),
                child: Text(
                  '合计',
                  style: TextStyle(
                    fontSize: ScreenUtil.getInstance().setSp(36),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                width: ScreenUtil.getInstance().setWidth(150),
                child: Text(
                  '￥$totalPrice',
                  style: TextStyle(
                    color: Colors.pink,
                    fontSize: ScreenUtil.getInstance().setSp(36),
                  ),
                ),
              )
            ],
          ),
          Container(
            width: ScreenUtil.getInstance().setWidth(430),
            alignment: Alignment.centerRight,
            child: Text(
              '满10元免配送费，预购免配送费',
              style: TextStyle(
                  color: Colors.black38,
                  fontSize: ScreenUtil.getInstance().setSp(22)),
            ),
          ),
        ],
      ),
    );
  }

  //结算组件
  Widget _goButton(context) {
    int totalGoodsCount = Provide.value<CartProvide>(context).totalGoodsCount;
    return Container(
      width: ScreenUtil.getInstance().setWidth(170),
      padding: const EdgeInsets.only(left: 10),
      child: InkWell(
        onTap: () {},
        child: Container(
          padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.pink,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          child: Text(
            '结算（$totalGoodsCount）',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
